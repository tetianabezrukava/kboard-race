import * as config from "./config";
import {changeArray} from './helpers';
import { texts } from '../data';
import { getRandomNumber } from './helpers';

const activeUsers = new Map();
const gamesMap = new Map();
let rooms = [];
let currentTextLength = '';
const newGamer = {
  status: 'unready',
  progress: 0,
  finishTime: 0
}

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    if (activeUsers.has(username)) {
      socket.emit("LOGIN_ERROR");
      socket.on("disconnect", () =>{});
    } else {
      if (!activeUsers.get(username)) {
        const userData = {
          currentRoom: ''
        }
  
        activeUsers.set(username, userData);
        socket.emit("LOAD_ROOM_PAGE", {
          username: username,
          rooms: rooms
        });
      }
    }
  
    socket.on("ADD_ROOM", (roomName) => {
      const searchingRoom = rooms.find((room) => room.roomName === roomName);
      if (searchingRoom) {
        socket.emit("ADD_ROOM_ERROR");
      } else {
        rooms.push({
          roomName: roomName,
          usersNumber: 0,
          isAvailable: true
        });
        socket.emit("ROOM_WAS_ADDED", roomName);
        socket.broadcast.emit("UPDATE_ROOMS", rooms)
      }
    });
    
    socket.on("JOIN_ROOM", (roomName) => {
      socket.join(roomName);
      activeUsers.set(username, {currentRoom: roomName})
      const searchingRoom = {...rooms.find((room) => room.roomName === roomName)};
      const idxOfSearchingRoom = rooms.findIndex((room) => room.roomName === roomName);
      searchingRoom.usersNumber += 1;
      if (searchingRoom.usersNumber === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        searchingRoom.isAvailable = false;
      }
      
      rooms = changeArray(rooms, searchingRoom, idxOfSearchingRoom);
      io.emit("UPDATE_ROOMS", rooms);
      
      if (gamesMap.has(roomName)) {
        const searchingGame = gamesMap.get(roomName);
        searchingGame[username] = { ...newGamer };
        gamesMap.set(roomName, searchingGame)
      } else {
        gamesMap.set(roomName, { [username]: { ...newGamer } })
      }

      const gameUsers = gamesMap.get(roomName);
      const gameData = {
        currentTextLength,
        users: gameUsers
      }
      socket.to(roomName).emit("UPDATE_PROGRESS_BAR", gameData);
    })
    
    socket.on("LEAVE_ROOM", (roomName) => {
      socket.leave(roomName);
      activeUsers.set(username, {currentRoom: ''})
      deleteUserFromRoom(io, socket, username, roomName);
    });
  
    socket.on("GET_DATA_FOR_GAME_PAGE", (roomName) => {
      const users = gamesMap.get(roomName);
      const data = {
        users,
        roomName,
        currentTextLength
      }
      socket.emit("LOAD_GAME_PAGE", data);
    })
    
    socket.on("CHANGE_STATUS", () => {
      const roomName = activeUsers.get(username).currentRoom;
      const searchingGame = gamesMap.get(roomName);
      const searchingUser = searchingGame[username];
      searchingUser.status = searchingUser.status === 'unready' ? 'ready' : 'unready';
      gamesMap.set(roomName, searchingGame);
      const gameUsers = gamesMap.get(roomName);
      const gameData = {
        currentTextLength,
        users: gameUsers
      }
      io.to(roomName).emit("UPDATE_PROGRESS_BAR", gameData);

      const gameUsersNames = Object.keys(gameUsers);
      const allUsersAreReady = gameUsersNames.every((user) => {
        return gameUsers[user].status === 'ready'
      });
      const searchingRoom = {...rooms.find((room) => room.roomName === roomName)};

      if (allUsersAreReady) {
        const idxOfSearchingRoom = rooms.findIndex((room) => room.roomName === roomName);
        searchingRoom.isAvailable = false;

        rooms = changeArray(rooms, searchingRoom, idxOfSearchingRoom)

        io.emit("UPDATE_ROOMS", rooms);

        const randomNumber = getRandomNumber(0, texts.length - 1);
        const randomText = texts[randomNumber];
        currentTextLength = randomText.length;

        const data = {
          secondsForPreparing: config.SECONDS_TIMER_BEFORE_START_GAME,
          secondsForGame: config.SECONDS_FOR_GAME,
          text: randomText
        }

        io.in(roomName).emit("PREPARE_FOR_GAME", data);


      } else {
        if (searchingRoom.usersNumber < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
          const idxOfSearchingRoom = rooms.findIndex((room) => room.roomName === roomName);
          searchingRoom.isAvailable = true;

          rooms = changeArray(rooms, searchingRoom, idxOfSearchingRoom);

          io.emit("UPDATE_ROOMS", rooms);
        }
      }
    })
  
    socket.on("UPDATE_PROGRESS", () => {
      const currentRoom = activeUsers.get(username).currentRoom;
      const searchingGame = gamesMap.get(currentRoom);
      const searchingUser = searchingGame[username];
      searchingUser.progress += 1;
      if (searchingUser.progress === currentTextLength) {
        searchingUser.finishTime = new Date();
      }
      gamesMap.set(currentRoom, searchingGame);
      const gameUsers = gamesMap.get(currentRoom);
      const gameData = {
        currentTextLength,
        users: gameUsers
      }
      io.to(currentRoom).emit("UPDATE_PROGRESS_BAR", gameData);
    })
    
    socket.on("GET_RESULT", () => {
      const currentRoom = activeUsers.get(username).currentRoom;
      const searchingRoom = {...rooms.find((room) => room.roomName === currentRoom)};
      if (searchingRoom.usersNumber < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        const idxOfSearchingRoom = rooms.findIndex((room) => room.roomName === currentRoom);
        searchingRoom.isAvailable = true;
  
        rooms = changeArray(rooms, searchingRoom, idxOfSearchingRoom);
  
        io.emit("UPDATE_ROOMS", rooms);
      }
      const searchingGame = gamesMap.get(currentRoom);
      const users = Object.keys(searchingGame);
      const finishedEarlie = users.filter((user) => searchingGame[user].finishTime > 0);
      const sortedFinishedEarlie = finishedEarlie.sort((userA, userB) => {
        return searchingGame[userA].finishTime - searchingGame[userB].finishTime
      });
      const finishedWithoutTime = users.filter((user) => searchingGame[user].finishTime === 0);
      const sortedFinishedWithoutTime = finishedWithoutTime.sort((userA, userB) => {
        return searchingGame[userB].progress - searchingGame[userA].progress
      });
      const places = [...sortedFinishedEarlie, ...sortedFinishedWithoutTime];
      if (users > 1) {
        socket.to(currentRoom).emit("LOG_RESULT", places);
      } else {
        socket.emit("LOG_RESULT", places);
      }
    });
    
    socket.on("DELETE_RESULT", () => {
      currentTextLength = '';
      const currentRoom = activeUsers.get(username).currentRoom;
      const currentGame = gamesMap.get(currentRoom);
      const users = Object.keys(currentGame);
      users.map((user) => {
        currentGame[user].status = 'unready';
        currentGame[user].progress = 0;
        currentGame[user].finishTime = 0;
      });
      activeUsers.set(currentRoom, currentGame);
      const gameUsers = gamesMap.get(currentRoom);
      const gameData = {
        currentTextLength: 100,
        users: gameUsers
      }
      io.to(currentRoom).emit("UPDATE_PROGRESS_BAR", gameData);
    })
    
    socket.on("disconnect", () => {
      const currentRoom = activeUsers.get(username).currentRoom;
      if (currentRoom) {
        deleteUserFromRoom(io, socket, username, currentRoom);
      }
      activeUsers.delete(username);
    });
  });
};

function deleteUserFromRoom(io, socket, username, currentRoom) {
  const searchingRoom = {...rooms.find((room) => room.roomName === currentRoom)};
  const idxOfSearchingRoom = rooms.findIndex((room) => room.roomName === currentRoom);
  searchingRoom.usersNumber -= 1;
  io.emit("UPDATE_ROOMS", rooms);
  
  if (!searchingRoom.usersNumber) {
    const idxOfEmptyRoom = rooms.findIndex((room) => room.usersNumber === 0);
    const copyOfRooms = [...rooms];
    copyOfRooms.splice(idxOfEmptyRoom, 1);
    rooms = copyOfRooms;
    io.emit("UPDATE_ROOMS", rooms);
    gamesMap.delete(currentRoom);
  } else {
    const searchingGame = gamesMap.get(currentRoom);
    delete searchingGame[username];
    gamesMap.set(currentRoom, searchingGame);
    const gameUsers = gamesMap.get(currentRoom);
    const gameData = {
      currentTextLength,
      users: gameUsers
    }
    socket.to(currentRoom).emit("UPDATE_PROGRESS_BAR", gameData);
    
    const gameUsersNames = Object.keys(gameUsers);
    const allUsersAreReady = gameUsersNames.every((user) => {
      return gameUsers[`${user}`].status === 'ready'
    });
    
    if (allUsersAreReady) {
      const randomNumber = getRandomNumber(0, texts.length - 1);
      const randomText = texts[randomNumber];
      currentTextLength = randomText.length;
      
      const data = {
        seconds: config.SECONDS_TIMER_BEFORE_START_GAME,
        text: randomText
      }
      socket.to(currentRoom).emit("PREPARE_FOR_GAME", data);
      searchingRoom.isAvailable = false;
      rooms = changeArray(rooms, searchingRoom, idxOfSearchingRoom);
      io.emit("UPDATE_ROOMS", rooms)
    } else {
      searchingRoom.isAvailable = true;
      rooms = changeArray(rooms, searchingRoom, idxOfSearchingRoom);
      socket.emit("UPDATE_ROOMS", rooms)
    }
  }
}