import { createElem, createReadyBtn } from '../helpers.mjs';
import { socket } from '../game.mjs';

export function prepareForGameListener({ secondsForPreparing, secondsForGame, text }) {
  const backToRoomsBtn = document.getElementById('quit-room-btn');
  backToRoomsBtn.style.visibility = 'hidden';
  const gameField = document.getElementById('game-field');
  gameField.innerHTML = '';
  const gameTimer = document.getElementById('game-timer');
  gameTimer.innerHTML = '';
  gameTimer.style.display = 'block';
  const timer = createElem('div');
  timer.setAttribute('id', 'timer');
  gameField.appendChild(timer);
  let time = secondsForPreparing;
  let counterForPreparing = setTimeout(function tick() {
    const value = time -= 1;
    timer.innerHTML = `${value}`;
    time = value;
    if (time < 1) {
      clearInterval(counterForPreparing);
      return;
    }
    counterForPreparing = setTimeout(tick, 1000)
  }, 1000)
  
  let prepareForGamePromise = new Promise(resolve => {
    let time = secondsForGame;
    setTimeout(() => {
      let counterForGame = setTimeout(function tick() {
        const value = time -= 1;
        gameTimer.innerHTML = `${value} seconds left`;
        time = value;
        if (time < 1) {
          clearInterval(counterForGame);
          return;
        }
        counterForGame = setTimeout(tick, 1000)
      }, 1000)
     return resolve(text)
    }, (secondsForPreparing + 1) * 1000);
  });
  
  prepareForGamePromise.then(data => {
    timer.style.display = 'none';
    gameField.innerHTML = '';
    const textContainer = document.createElement('div');
    textContainer.setAttribute('id', 'text-container');
    textContainer.innerHTML = `${data}`;
    gameField.appendChild(textContainer);
    game(secondsForGame);
  });
}

function game(secondsForGame) {
  const textContainer = document.getElementById('text-container');
  const iOpenTag = '<i class="underlined-char">'
  const iCloseTag = '</i>';
  const spanOpenTag = '<span class="marked-text">';
  const spanCloseTag = '</span>';
  const startContent = textContainer.innerHTML;
  const underlinedChar = startContent[0];
  const otherText = startContent.slice(1);
  textContainer.innerHTML = `${iOpenTag}${underlinedChar}${iCloseTag}${otherText}`
  
  let i = 0;
  
  window.addEventListener('keydown', (ev) => keyHandler(ev));
  
  let finishGamePromise = new Promise(resolve => {
    setTimeout(() => {
      window.removeEventListener('keydown', (ev) => keyHandler(ev));
      socket.emit("GET_RESULT");
      resolve()
    }, secondsForGame * 1000);
  })
  
  finishGamePromise.then()
  
  function keyHandler(ev) {
    let content = textContainer.innerHTML
  
    const markedBlock = document.querySelector('.marked-text');
    let markedText;
    markedBlock ? markedText = markedBlock.innerHTML : markedText = '';
  
    const underlinedBlock = document.querySelector('.underlined-char');
    const underlinedChar = underlinedBlock.innerHTML;
  
    const otherTextIdx = content.indexOf(iCloseTag) + iCloseTag.length;
    const otherText = content.slice(otherTextIdx);
  
    content = `${markedText}${underlinedChar}${otherText}`
  
    if (content[i] === ev.key) {
      socket.emit('UPDATE_PROGRESS');
      
      const markedText = content.slice(0, i + 1);
      const otherText = content.slice(i + 1);
      const newContent = `${spanOpenTag}${markedText}${spanCloseTag}${otherText}`;
      const endIdxSpanTag = newContent.indexOf(spanCloseTag) + spanCloseTag.length;
      const spanWithContent = newContent.slice(0, endIdxSpanTag);
      const underlinedChar = newContent.slice(endIdxSpanTag, endIdxSpanTag + 1);
      const newOtherText = otherText.slice(1);
      textContainer.innerHTML = `${spanWithContent}${iOpenTag}${underlinedChar}${iCloseTag}${newOtherText}`
      i++;
    }
  }
}

export function logGameResultListener(results) {
  const gameTimer = document.getElementById('game-timer');
  gameTimer.style.display = 'none';
  socket.emit('DELETE_RESULT');
  const gameResults = document.getElementById('game-results');
  gameResults.innerHTML = '';
  let i = 1;
  results.map((result) => {
    const place = document.createElement('p');
    place.innerHTML = `${i} place - ${result}`;
    gameResults.appendChild(place);
    i++
  })
  const modal = document.getElementById('modal');
  modal.style.display = 'block'
  const closeBtn = document.getElementById('quit-results-btn');
  closeBtn.addEventListener('click', () => onCloseBtnClick(modal), {once: true})
}

function onCloseBtnClick(modal) {
  modal.style.display = 'none';
  const textContainer = document.getElementById('text-container');
  textContainer.innerHTML = '';
  const backToRoomsBtn = document.getElementById('quit-room-btn');
  backToRoomsBtn.style.visibility = 'visible';
  const readyBtn = createReadyBtn();
  const gameField = document.getElementById('game-field');
  gameField.appendChild(readyBtn);
}