import { socket, username } from './game.mjs';
export function createElem(elem) {
  return document.createElement(`${elem}`)
}

export function createRoom(roomName, usersNumber) {
  const room = createElem('div');
  room.classList.add('room');
 
  const note = createElem('span');
  const word = usersNumber === 1 ? 'user' : 'users'
  note.innerHTML = `${usersNumber} ${word} connected`;
  
  const title = createElem('h6');
  title.innerHTML = `Room ${roomName}`;
  
  const btn = createElem('button');
  btn.classList.add('join-btn');
  btn.setAttribute('type', 'button');
  btn.innerHTML='Join';
  btn.addEventListener('click', () => onJoinClick(roomName));
  
  room.append(note, title, btn);
  
  return room
}

export function createGamePlayer({ name, status, progress, currentTextLength, isCurrentUser }) {
  const player = document.createElement('li');
  
  const statusMark = document.createElement('span');
  const statusClass = status === 'ready' ? 'ready-status-green' : 'ready-status-red';
  statusMark.classList.add('status-mark', statusClass);
  
  const playerName = document.createElement('span');
  playerName.innerHTML = `${name}`;
  
  const nameMark = document.createElement('span');
  nameMark.classList.add('name-mark');
  nameMark.innerHTML = '(you)';
  nameMark.style.display = isCurrentUser ? 'inline' : 'none';
  
  const progressBar = document.createElement('progress');
  progressBar.setAttribute('max', `${currentTextLength}`);
  progressBar.setAttribute('value', `${progress}`);
  
  player.append(statusMark, playerName, nameMark, progressBar);
  return player;
}

export function createReadyBtn() {
  const readyBnt = document.createElement('button');
  readyBnt.setAttribute('id', 'ready-btn');
  readyBnt.setAttribute('type', 'button');
  readyBnt.innerHTML = 'ready';
  readyBnt.addEventListener('click', () => onReadyClick());
  return readyBnt;
}

function onJoinClick(roomName) {
  socket.emit("JOIN_ROOM", roomName);
  socket.emit("GET_DATA_FOR_GAME_PAGE", roomName);
  switchToGamePage();
}

export function switchToGamePage() {
  const roomsPage = document.getElementById('rooms-page');
  roomsPage.classList.add('display-none');
  const gamePage = document.getElementById('game-page');
  gamePage.classList.replace('display-none', 'flex');
}

export function switchToRoomsPage() {
  const gamePage = document.getElementById('game-page');
  gamePage.classList.replace('flex', 'display-none')
  const roomsPage = document.getElementById('rooms-page');
  roomsPage.classList.remove('display-none');
}

function onReadyClick() {
  socket.emit("CHANGE_STATUS")
  const readyBnt = document.getElementById('ready-btn');
  readyBnt.innerHTML  = readyBnt.innerHTML === 'ready' ? 'unready' : 'ready';
}